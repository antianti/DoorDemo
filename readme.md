Video: https://www.youtube.com/watch?v=wDXrTFSc2hU

-- How to migrate a door blueprint to an existing project --

1.	Create a new Door project via Launcher.
2.	Open PlayerInterfaceLibrary Functions Library and clear all internal nodes in all functions (only keep start nodes and return nodes).
3.	Make sure you project doesn't have conflicting names like IActorInteractive interface, PlayerInterfaceLibrary functions library and EControllerButton, EDoorRotationState, EHandPose. Otherwise, rename assets before migration.
4.	Right Mouse click at the BP_RotatingDoor blueprint in the Content Browser and select 'Migrate', then select Content folder of your project. Close DoorDemo project.
5.	In your project, open PlayerInterfaceLibrary Functions Library and reimplement all functions for your player pawn blueprint. This functions should return references to motion controller components, hand mesh components (either static mesh or skeletal mesh) and hand trigger components (colliders attached to motion controllers, but hand mesh components can be used to detect collision too).
6.	Make sure that hand colliders in your project can overlap WorldDynamic objects and generate overlap events (Collision rollout in the Details panel).
7.	Add IActorInteractive interface support to your player pawn blueprint, at least for MotionControllerButtonPress and MotionControllerButtonEvents. See example in the MotionControllerPawn blueprint in the DoorDemo project. Note that you can only add support for a single events used to control door pulling in your project (like Grip or Trigger button).
8.	Open BP_RotatingDoor blueprint, change door and/or handle static meshes and adjust colliders’ positions.
9.	Set preferable properties in the BP_RotatingDoor settings (Setup subcategoty).


-- Blueprint Settings --

* YawLimit1 and YawLimit2 are rotation limits in the relative space. 0 and 90 degrees mean door rotating around left axis, set to -90 and 0 for right door (YawLimit1 must be lower than YawLimit2).
* YawLimitOpen is a limit which makes actor believe that door is open (ignore it if you don't use OnDoorOpened event).
* StopControlDistance is a maximum acceptable distance from motion controller component to door handle when player is pulling a door.
* Uncheck YawLimit1isClosed if YawLimit2 means closed door and YawLimit1 is for open door.
* PullButton is a motion controller button used to activate pulling (usually Grip or Trigger).
* DoorPullingHandPose is a hand pose updated by blueprint if UpdateHandPoseOnPulling is checked.
* Set UpdateHandPoseOnPulling if door blueprint should update hand pose when player start/stop door pulling.
* Uncheck RequireHandPoseForPushing if you want to allow pushing for any hand pose.
* DoorPushingHandPose is a hand pose required to start pushing if RequireHandPoseForPushing is checked.
* RotationSoundAsset and HitSoundAsset are sounds used for door rotation and hits. Set Looping to true in the rotation sound settings.

-- Functions and Events --

* OnDoorOpened event called when door cross YawLimitOpen in a direction of opened yaw limit (see YawLimit1isClosed) and.
* OnDoorClosed event called when door is fully closed.
* IsDoorOpen() function returns true if current door rotation is between open yaw limit and YawLimitOpen.